import { useState, useEffect } from 'react'
import Pedidos from './Pedidos'
import VerPedidos from './VerPedidos'
import VistaAdmin from './VistaAdmin'

const Home = ({ onLogout, userId }) => {
    const [user, setUser] = useState()

    useEffect(() => {
        fetch('http://localhost:8000/api/users/' + userId, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
        })
        .then((res) => res.json())
        .then((userData) => {
            setUser(userData)
        })
    }, [])

    const logoutHandler = () => {
        onLogout()
    }

    const role = user ? user.group_name : null

    return (
        <>
            <button className='logout-button button' onClick={logoutHandler}>Logout</button>
            {!user && <p>Loading...</p>}
            {role && role === 'recepcionista' && <Pedidos user={user}/>}
            {role && role === 'administrador' && <VistaAdmin/>}
            {role && role === 'jefe_de_cocina' && <VerPedidos user={user}/>}
        </>
    )
}

export default Home
