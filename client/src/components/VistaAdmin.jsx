import React from 'react'
import { useState } from 'react'
import UsuarioForm from './UsuarioForm'
import VerUsuarios from './VerUsuarios'
import ProductoForm from './ProductoForm'
import VerProductos from './VerProductos'

const VistaAdmin = () => {
    
    const [seccionActiva, setSeccionActiva] = useState('')
    const [usuarioSeleccionado, setUsuarioSeleccionado] = useState({})
    const [productoSeleccionado, setProductoSeleccionado] = useState({})

    return (
        <div className='width-80'>
            <div className='flex-container'>
                {
                    seccionActiva === 'Crear Usuario' ?
                    <button className='flex-1 seccion-seleccionada'>Crear Usuario</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('Crear Usuario')}>Crear Usuario</button>
                }
                {
                    seccionActiva === 'verUsuarios' || seccionActiva === 'Editar Usuario' ?
                    <button className='flex-1 seccion-seleccionada' onClick={()=>setSeccionActiva('verUsuarios')}>Ver Usuarios</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('verUsuarios')}>Ver Usuarios</button>
                }
                {
                    seccionActiva === 'Crear Producto' ?
                    <button className='flex-1 seccion-seleccionada'>Crear Producto</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('Crear Producto')}>Crear Producto</button>
                }
                {
                    seccionActiva === 'verProductos' || seccionActiva === 'Editar Producto' ?
                    <button className='flex-1 seccion-seleccionada' onClick={()=>setSeccionActiva('verProductos')}>Ver Productos</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('verProductos')}>Ver Productos</button>
                }
            </div>
            {seccionActiva === 'Crear Usuario' && <UsuarioForm seccionActiva={seccionActiva}/>}
            {seccionActiva === 'verUsuarios' && <VerUsuarios setSeccionActiva={setSeccionActiva} setUsuarioSeleccionado={setUsuarioSeleccionado}/>}
            {seccionActiva === 'Editar Usuario' && <UsuarioForm seccionActiva={seccionActiva} usuarioSeleccionado={usuarioSeleccionado}/>}
            {seccionActiva === 'Crear Producto' && <ProductoForm seccionActiva={seccionActiva}/>}
            {seccionActiva === 'verProductos' && <VerProductos setSeccionActiva={setSeccionActiva} setProductoSeleccionado={setProductoSeleccionado}/>}
            {seccionActiva === 'Editar Producto' && <ProductoForm seccionActiva={seccionActiva} productoSeleccionado={productoSeleccionado}/>}
        </div>
    )
}

export default VistaAdmin