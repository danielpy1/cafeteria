import React from 'react'
import { useState, useEffect } from 'react'
import Modal from './Modal'

const VerUsuarios = (props) => {

    const [usuarios, setUsuarios] = useState([])
    const { setSeccionActiva, setUsuarioSeleccionado } = props
    const [modal, setModal] = useState(false)
    const [opcion, setOpcion] = useState("")
    const [id, setId] = useState("")

    useEffect(() => {
        fetch('http://localhost:8000/api/users/', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
        .then((res) => res.json())
        .then((usersData) => {
            setUsuarios(usersData)
        })
    }, [])

    useEffect(() => {
        if (opcion === "aceptar"){
            fetch('http://localhost:8000/api/users/'+id+'/', {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
            })
            setUsuarios(usuarios.filter( el => el.id !== id))
            setOpcion("")
        }
    }, [opcion])

    const scroll = {
        overflowY: 'auto',
    }
    const fit = {
        height: 'fit-content',
        margin: '2vh auto',
        width: '70vw',
    }

    return (
        <>
            <div id='main-container' style={scroll}>
                <table style={fit}>
                    <thead>
                        <tr>
                            <th colSpan={5}>Listado de usuarios</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Nombre de usuario</th>
                            <th>Rol</th>
                            <th colSpan={2}>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            usuarios.map( (value, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{value.id}</td>
                                        <td>{value.username}</td>
                                        <td>{value.group_name}</td>
                                        <td>
                                            <button onClick={() => {setUsuarioSeleccionado(value);setSeccionActiva('Editar Usuario')}}>Editar</button>
                                        </td>
                                        <td>
                                            <button onClick={() => {setId(value.id);setModal(true)}}>Eliminar</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            {
                modal ?
                    <Modal setModal={setModal} setOpcion={setOpcion}/>
                :
                    ""
            }
        </>
    )
}

export default VerUsuarios