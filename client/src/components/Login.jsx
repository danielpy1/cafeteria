import React from 'react'
import { useState } from 'react'
import jwtDecode from 'jwt-decode'

const Login = ({ onLogin }) => {
    const [usuario, setUsuario] = useState("")
    const [contrasenha, setContrasenha] = useState("")
    const [error, setError] = useState(null)

    const handleOnSubmit = (e) => {
        e.preventDefault()
        fetch('http://localhost:8000/api/login/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                username: usuario,
                password: contrasenha,
            }),
        })
        .then((res) => res.json())
        .then((tokenData) => {
            if(tokenData){
                window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
                onLogin(jwtDecode(tokenData.access).user_id)
            }
        })
        .catch((err) => {
            if (err.message === 'Invalid token specified') {
                setError('Usuario o contraseña incorrectos')
            }
        })
    }

    return (
        <form id='login-form' onSubmit={handleOnSubmit}>
            <h1>Coffee Time</h1>
            <div className='flex-container label-input-div'>
                <label className='flex-1'>Usuario:</label>
                <input className='flex-2' type="text" value={usuario} onChange={(e)=>setUsuario(e.target.value)}/>
            </div>
            <div className='flex-container label-input-div'>
                <label className='flex-1'>Contraseña:</label>
                <input className='flex-2' type="password" value={contrasenha} onChange={(e)=>setContrasenha(e.target.value)}/>
            </div>
            {error && <p className='error'>{error}</p>}
            <button className='button'>Iniciar Sesión</button>
        </form>
    )
}

export default Login