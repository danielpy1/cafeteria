import React from 'react'
import { useState, useEffect } from 'react'
import Modal from './Modal'

const VerPedidos = ({ user }) => {
  const [pedidos, setPedidos] = useState([])
  const [modal, setModal] = useState(false)
  const [id, setId] = useState("")
  const [pedidoGuardado, setPedidoGuardado] = useState({})
  const [opcion, setOpcion] = useState("")

  useEffect(() => {
    
    if (user.group_name === "jefe_de_cocina"){
      getPedidosPorPreparar()
    }else if (user.group_name === "recepcionista"){
      getPedidosListos()
    }
  }, [user])

  useEffect(() => {
    if (opcion === "aceptar"){
      if ( user.group_name === "jefe_de_cocina"){
        handleListo(id)
      }else if (user.group_name === "recepcionista"){
        handleEntregar(id)
      }
      setOpcion("")
    }
    setId("")
  }, [opcion])
  
  

  const getPedidosListos = () => {
    fetch('http://localhost:8000/api/pedidos/', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
    })
    .then((res) => res.json())
    .then((pedidosData) => {
      console.log(pedidosData);
      
      pedidosData.forEach(pedido => {
        pedido.lista_productos = convertirAJSON(pedido.lista_productos)
      });

      fetch('http://localhost:8000/api/productos/', {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
              'Content-Type': 'application/json',
          },
      })
      .then((res) => res.json())
      .then((productosData) => {
        const aux = pedidosData.filter( el => el.estado === "listo").map( el => {
          return (
            {
              ...el,
              lista_productos: el.lista_productos.map( producto => {
                return (
                  {
                    ...producto,
                    nombre: productosData.filter( p => p.id === producto.id)[0].nombre
                  }
                )
              }),
            }
          )
        })
        setPedidos(aux)
      })
    })
  }

  const getPedidosPorPreparar = () => {
    fetch('http://localhost:8000/api/pedidos/', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
    })
    .then((res) => res.json())
    .then((pedidosData) => {
      console.log(pedidosData);

      pedidosData.forEach(pedido => {
        pedido.lista_productos = convertirAJSON(pedido.lista_productos)
      });

      fetch('http://localhost:8000/api/productos/', {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
              'Content-Type': 'application/json',
          },
      })
      .then((res) => res.json())
      .then((productosData) => {
        const aux = pedidosData.filter( el => el.estado === "pendiente").map( el => {
          return (
            {
              ...el,
              lista_productos: el.lista_productos.map( producto => {
                return (
                  {
                    ...producto,
                    nombre: productosData.filter( p => p.id === producto.id)[0].nombre
                  }
                )
              }),
            }
          )
        })
        setPedidos(aux)
      })
    })
  }

  const convertirAJSON = (string) => {

    // Remueve todos los "OrderedDict", envuelve el contenido con llaves "{}" y elimina parentesis
    const stringLimpio = string.replace(/OrderedDict\(\[(.*?)\]\)/g, '{$1}').replace(/[()]/g, '');

    // Cambia las comillas y posiciona los ":" para obtener una cadena JSON vÃ¡lida
    const jsonString = stringLimpio.replace(/'/g, '"').replace(/("(?:[^"]|\\")*"),/g, '$1:');

    // Convertir la cadena JSON en un objeto JavaScript
    const jsonData = JSON.parse(jsonString);

    return jsonData
  }

  const handleListo = (id) => {

    setPedidoGuardado({
      ...pedidoGuardado,
      lista_productos: pedidoGuardado.lista_productos.map( producto => {
        delete producto.nombre
        return producto
      })
    })

    fetch('http://localhost:8000/api/pedidos/'+id+"/", {
        method: 'PUT',
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id,
          nombre_cliente: pedidoGuardado.nombre_cliente,
          lista_productos: pedidoGuardado.lista_productos,
          estado: 'listo'
        }),
    })
    .then((res) => res.json())
    console.log("Pedido listo: " + id)
    setPedidos(pedidos.filter( el => el.id !== id))
  }

  const handleEntregar = (id) => {

    setPedidoGuardado({
      ...pedidoGuardado,
      lista_productos: pedidoGuardado.lista_productos.map( producto => {
        delete producto.nombre
        return producto
      })
    })

    fetch('http://localhost:8000/api/pedidos/'+id+"/", {
        method: 'PUT',
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id,
          nombre_cliente: pedidoGuardado.nombre_cliente,
          lista_productos: pedidoGuardado.lista_productos,
          estado: 'entregado'
        }),
    })
    .then((res) => res.json())
    console.log("Pedido entregado: " + id)
    setPedidos(pedidos.filter( el => el.id !== id))
  }

  const mostrarModal = (pedido) => {
    setPedidoGuardado(pedido)
    setId(pedido.id)
    setModal(true)
    console.log(pedido)
  }

  const estiloContenedorExterior = {
    overflowX: "auto",
    overflowY: "auto"
  }
  const estiloContenedorPedido = {
    border: "1px solid red",
    backgroundColor: "white",
    padding: "1em",
    margin: "1em",
    width: "17em",
    height: "fit-content",
  }
  const estiloTabla = {
    width: '15em',
  }
  const estiloSinPedidos = {
    textAlign: 'center',
    padding: '4vh 0',
    width: '100%'
  }

  return (
    <>
      <div id="main-container" style={estiloContenedorExterior}>
        {
          pedidos.length > 0 ?
            pedidos.map( (value, index) => {
              return (
                <div key={index} style={estiloContenedorPedido}>
                  <table style={estiloTabla}>
                    <thead>
                      <tr>
                        <th colSpan={2}>Pedido {value.id}</th>
                      </tr>
                      <tr>
                        <th colSpan={2}>{value.nombre_cliente}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        value.lista_productos.map( (producto, index) => {
                          return (
                            <tr key={index}>
                              <td>{producto.nombre}</td>
                              <td>{producto.cantidad}</td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colSpan={2}>
                          {
                            user.group_name === "jefe_de_cocina" ?
                              <button onClick={()=>mostrarModal(value)}>Listo</button>
                            :
                              ""
                          }
                          {
                            user.group_name  === "recepcionista" ?
                              <button onClick={()=>mostrarModal(value)}>Entregar</button>
                            :
                              ""
                          }
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              )
            })
          :
            <div style={estiloSinPedidos}>
              No hay pedidos por mostrar
            </div>
        }
      </div>
      {
        modal ?
          <Modal setModal={setModal} setOpcion = {setOpcion}/>
        :
          ""
      }
    </>
  )
}

export default VerPedidos