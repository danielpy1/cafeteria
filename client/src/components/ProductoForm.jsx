import React, { useState } from 'react'
import SuccessModal from './SuccessModal'

const ProductoForm = (props) => {

    const { seccionActiva, productoSeleccionado } = props
    
    const getNombre = () => {
        if (seccionActiva === 'Editar Producto') {
            return productoSeleccionado.nombre
        }
        return ""
    }

    const getPrecio = () => {
        if (seccionActiva === 'Editar Producto') {
            return productoSeleccionado.precio
        }
        return ""
    }

    const [nombre, setNombre] = useState(()=>getNombre())    
    const [precio, setPrecio] = useState(()=>getPrecio())
    const [modal, setModal] = useState(false)

    const handleOnClick = (e) => {
        e.preventDefault()
        if (seccionActiva === 'Editar Producto') {
            fetch('http://localhost:8000/api/productos/'+productoSeleccionado.id+"/", {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: productoSeleccionado.id,
                    nombre,
                    precio,
                }),
            })
            .then((res) => res.json())
        }else{
            fetch('http://localhost:8000/api/productos/', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    nombre,
                    precio,
                }),
            })
            .then((res) => res.json())
        }
        setModal(true)
        setNombre("")
        setPrecio("")
    }

    return (
        <>
            <div id='main-container'>
                <form className='gestion-form'>
                    <h2>{seccionActiva}</h2>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='nombre'>Nombre</label>
                        <input
                            className='flex-2'
                            type='text'
                            id='nombre'
                            value={nombre}
                            onChange={(e)=>setNombre(e.target.value)}
                            placeholder='Nombre del producto'
                            required
                        />
                    </div>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='precio'>Precio</label>
                        <input
                            className='flex-2'
                            type='number'
                            id='precio'
                            value={precio}
                            onChange={(e)=>setPrecio(e.target.value)}
                            placeholder='Precio del producto'
                            required
                        />
                    </div>
                    <div className='button-div'>
                        <button className='button' type='submit' onClick={(e)=>handleOnClick(e)}>Guardar</button>
                    </div>
                </form>
            </div>
            {
                modal ?
                    <SuccessModal setModal = {setModal}/>
                :
                    null
            }
        </>
    )
}

export default ProductoForm