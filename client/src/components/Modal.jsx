import React from 'react'
import "../Modal.css"

const Modal = (props) => {

    const { setModal, setOpcion } = props

    const closeModal = function () {
        setModal(false)
    }

    const aceptar = function () {
        setModal(false)
        setOpcion("aceptar")
    }

    const cancelar = function () {
        setModal(false)
        setOpcion("cancelar")
    }

    return (
        <>
            <section className="modal">
                <div>
                    <h3>
                        ¿Está seguro que desea realizar esta acción?
                    </h3>
                </div>
                <div>
                    <button className="accept-btn btn" onClick={()=>aceptar()}>Aceptar</button>
                    <button className="cancel-btn btn" onClick={()=>cancelar()}>Cancelar</button>
                </div>
            </section>
            <div className="overlay" onClick={()=>closeModal()}></div>
        </>
    )
}

export default Modal