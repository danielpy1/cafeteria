import React, { useState } from 'react'
import SuccessModal from './SuccessModal'

const UsuarioForm = (props) => {

    const { seccionActiva, usuarioSeleccionado } = props
    
    const getUsername = () => {
        if (seccionActiva === 'Editar Usuario') {
            return usuarioSeleccionado.username
        }
        return ""
    }

    const getRol = () => {
        if (seccionActiva === 'Editar Usuario') {
            return usuarioSeleccionado.group_name
        }
        return ""
    }

    const [username, setUsername] = useState(()=>getUsername())    
    const [rol, setRol] = useState(()=>getRol())
    const [modal, setModal] = useState(false)
    const [contrasenha, setContrasenha] = useState('')
    const [confirmarContrasenha, setConfirmarContrasenha] = useState('')

    const handleOnClick = (e) => {
        e.preventDefault()
        if (seccionActiva === 'Editar Usuario') {
            fetch('http://localhost:8000/api/users/'+usuarioSeleccionado.id+"/", {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: usuarioSeleccionado.id,
                    username,
                    password: contrasenha,
                    group_name: rol,
                }),
            })
            .then((res) => res.json())
        }else{
            fetch('http://localhost:8000/api/users/', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username,
                    password: contrasenha,
                    group_name: rol,
                }),
            })
            .then((res) => res.json())
        }
        setModal(true)
        setUsername("")
        setRol("")
        setContrasenha("")
        setConfirmarContrasenha("")
    }

    return (
        <>
            <div id='main-container'>
                <form className='gestion-form'>
                    <h2>{seccionActiva}</h2>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='username'>Nombre de usuario</label>
                        <input
                            className='flex-2'
                            type='text'
                            id='username'
                            value={username}
                            onChange={(e)=>setUsername(e.target.value)}
                            placeholder='Nombre del usuario'
                            required
                        />
                    </div>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='rol'>Rol</label>
                        <input
                            className='flex-2'
                            type='text'
                            id='rol'
                            value={rol}
                            onChange={(e)=>setRol(e.target.value)}
                            placeholder='Rol del usuario'
                            required
                        />
                    </div>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='contrasenha'>Contraseña</label>
                        <input
                            className='flex-2'
                            type='password'
                            id='contrasenha'
                            value={contrasenha}
                            onChange={(e)=>setContrasenha(e.target.value)}
                            required
                        />
                    </div>
                    <div className='flex-container label-input-div'>
                        <label className='flex-1' htmlFor='contrasenha'>Confirmar Contraseña</label>
                        <input
                            className='flex-2'
                            type='password'
                            id='confirmar-contrasenha'
                            value={confirmarContrasenha}
                            onChange={(e)=>setConfirmarContrasenha(e.target.value)}
                            required
                        />
                    </div>
                    <div className='button-div'>
                        <button className='button' type='submit' onClick={(e)=>handleOnClick(e)}>Guardar</button>
                    </div>
                </form>
            </div>
            {
                modal ?
                    <SuccessModal setModal = {setModal}/>
                :
                    null
            }
        </>
    )
}

export default UsuarioForm