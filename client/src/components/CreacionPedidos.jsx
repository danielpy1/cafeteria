import React from 'react'
import { useState, useEffect } from 'react'

const estiloColumnaGrande = {
    width: '30vw',
    padding: '1vh 1vw',
    
}
const estiloMargen = {
    marginRight: '1vw'
}
const estiloCampoCantidad = {
    maxWidth: '5vw',
    margin: '0'
}
const estiloContenedorSeleccionado = {
    margin: '2vh 0',
}

const CreacionPedidos = () => {
    const [pedido, setPedido] = useState([])
    const [productos, setProductos] = useState(null)
    const [cliente, setCliente] = useState('')

    useEffect(() => {
        fetch('http://localhost:8000/api/productos/', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
        .then((res) => res.json())
        .then((productosData) => {
            setProductos(productosData)
        })
    }, [])

    const handleCambioProducto = (producto) => {
        const aux = pedido
        if (!aux.some(el => el.nombre === producto.nombre)){
            aux.push({
                id: producto.id,
                nombre: producto.nombre,
                precio: producto.precio,
                cantidad: 1
            })
            setPedido([...aux])
        }else{
            aux.splice(aux.findIndex( el => el.nombre === producto.nombre),1)
            setPedido([...aux])
        }
    }

    const handleGenerarPedido = () => {
        const arrayFinal = []
        for (let i = 0; i < pedido.length; i++) {
            arrayFinal.push({
                id: pedido[i].id,
                cantidad: pedido[i].cantidad,
            })
        }
        fetch('http://localhost:8000/api/pedidos/', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                nombre_cliente: cliente,
                lista_productos: arrayFinal,
                estado: 'pendiente',
            }),
        })
        .then((res) => res.json())
        setPedido([])
        setCliente('')
    }

    const handleCambioCantidad = (e, value) => {
        const aux = pedido
        if (e.target.value === '')
            aux[aux.findIndex( el => el.nombre === value.nombre)].cantidad = 1
        else if (e.target.value < 1)
            aux.splice(aux.findIndex( el => el.nombre === value.nombre),1)
        else
            aux[aux.findIndex( el => el.nombre === value.nombre)].cantidad = e.target.value
        setPedido([...aux])
    }

    let total = 0

    return (
        <div id='main-container'>
            <div id='pedidos-lista-productos'>
                {
                    productos ?
                        productos.map( (value, index) => {
                            if(pedido.some( el => el.nombre === value.nombre))
                                return (
                                    <div style={estiloContenedorSeleccionado} key={index} className='flex-container'>
                                        <div className='flex-3 producto-seleccionado' onClick={()=>handleCambioProducto(value)}>
                                            <p>{value.nombre}</p>
                                        </div>
                                        <div style={estiloMargen} className='flex-1'>
                                            <input style={estiloCampoCantidad} type="number" onChange={(e)=>handleCambioCantidad(e,value)}/>
                                        </div>
                                    </div>
                                )
                            else
                                return (
                                    <div key={index} className='pedido-producto-listado' onClick={()=>handleCambioProducto(value)}>
                                        <p>{value.nombre}</p>
                                    </div>
                                )
                        })
                        :
                        "Cargando..."
                }
            </div>
            <div id='pedidos-lista-pedido'>
                <div id='pedidos-bloque'>
                    <table>
                        <thead>
                            <tr>
                                <th style={estiloColumnaGrande}>Nombre</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            pedido.length > 0 ?
                                pedido.map( (value, index) => {
                                    total += (value.precio * value.cantidad)
                                    return (
                                        <tr key={index}>
                                            <td style={estiloColumnaGrande} className=''>{value.nombre}</td>
                                            <td className=''>{value.cantidad}</td>
                                            <td className=''>{value.precio}</td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={3}>No hay productos en el pedido</td>
                                </tr>
                        }
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan={2}>Total</td>
                                <td>{total}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div className='flex-container'>
                    <div className='flex-3'>
                        <label>Cliente:</label>
                        <input type="text" value={cliente} onChange={(e)=>setCliente(e.target.value)}/>
                    </div>
                    <button id='pedidos-generar-button' className='flex-1' onClick={handleGenerarPedido}>Generar Pedido</button>
                </div>
            </div>
        </div>
    )
}

export default CreacionPedidos