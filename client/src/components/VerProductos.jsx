import React from 'react'
import { useState, useEffect } from 'react'
import Modal from './Modal'

const VerProductos = (props) => {

    const [productos, setProductos] = useState([])
    const { setSeccionActiva, setProductoSeleccionado } = props
    const [modal, setModal] = useState(false)
    const [opcion, setOpcion] = useState("")
    const [id, setId] = useState("")

    useEffect(() => {
        fetch('http://localhost:8000/api/productos/', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
        .then((res) => res.json())
        .then((productosData) => {
            setProductos(productosData)
        })
    }, [])

    useEffect(() => {
        if (opcion === "aceptar"){
            fetch('http://localhost:8000/api/productos/'+id+'/', {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
            })
            setProductos(productos.filter( el => el.id !== id))
            setOpcion("")
        }
    }, [opcion])

    const scroll = {
        overflowY: 'auto',
    }
    const fit = {
        height: 'fit-content',
        margin: '2vh auto',
        width: '70vw',
    }

    return (
        <>
            <div id='main-container' style={scroll}>
                <table style={fit}>
                    <thead>
                        <tr>
                            <th colSpan={5}>Listado de productos</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th colSpan={2}>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            productos.map( (value, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{value.id}</td>
                                        <td>{value.nombre}</td>
                                        <td>{value.precio}</td>
                                        <td>
                                            <button onClick={() => {setProductoSeleccionado(value);setSeccionActiva('Editar Producto')}}>Editar</button>
                                        </td>
                                        <td>
                                            <button onClick={() => {setId(value.id);setModal(true)}}>Eliminar</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            {
                modal ?
                    <Modal setModal={setModal} setOpcion={setOpcion}/>
                :
                    ""
            }
        </>
    )
}

export default VerProductos