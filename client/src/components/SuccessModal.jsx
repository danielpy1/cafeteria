import React from 'react'
import "../Modal.css"

const SuccessModal = (props) => {

    const { setModal } = props

    const closeModal = function () {
        setModal(false)
    }

    return (
        <>
            <section className="modal">
                <div>
                    <h3>
                        ¡Proceso completado con éxito!
                    </h3>
                </div>
                <div>
                    <button className="accept-btn btn" onClick={()=>closeModal()}>Aceptar</button>
                </div>
            </section>
            <div className="overlay" onClick={()=>closeModal()}></div>
        </>
    )
}

export default SuccessModal