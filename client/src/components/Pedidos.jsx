import React from 'react'
import { useState } from 'react'
import CreacionPedidos from './CreacionPedidos'
import VerPedidos from './VerPedidos'

const Pedidos = ({ user }) => {
    const [seccionActiva, setSeccionActiva] = useState('')

    return (
        <div className='width-80'>
            <div className='flex-container'>
                {
                    seccionActiva === 'creacionPedidos' ?
                    <button className='flex-1 seccion-seleccionada'>Generar Pedido</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('creacionPedidos')}>Generar Pedido</button>
                }
                {
                    seccionActiva === 'visualizacionPedidos' ?
                    <button className='flex-1 seccion-seleccionada'>Ver Pedidos</button>
                    :
                    <button className='flex-1' onClick={()=>setSeccionActiva('visualizacionPedidos')}>Ver Pedidos</button>
                }
            </div>
            {seccionActiva === 'creacionPedidos' && <CreacionPedidos/>}
            {seccionActiva === 'visualizacionPedidos' && <VerPedidos user={user}/>}
        </div>
    )
}

export default Pedidos