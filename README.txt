PROYECTO: CAFETERIA [ ReactJS - Django REST ]

# Levantar el frontent
	-Correr el comando npm install
	-Correr el comando npm run dev en la carpeta client

# Levantar el backend
- Crear un entorno virtual:
	python -m virtualenv .venv
- Activar el entorno virtual:
	.venv\Scripts\activate 
- Instalar las dependencias
	pip install -r requirements.txt
- Preparar las migraciones para la base de datos:
	 python .\manage.py makemigrations
- Realizar las migraciones:
	python .\manage.py migrate

# En mi caso no logré crear los usuarios y con roles asi que para proseguir con la prueba necesariamente se tiene que crear de forma manual ya sea directamente en la base de datos o con el dj-admin:

ROLES/GRUPOS:
-administrador
-jefe_de_cocina
-recepcionista
	