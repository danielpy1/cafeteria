from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from backend.permissions import IsRecepcionistaOrIsAdministradorOrIsJefeDeCocina, IsAdministrador

class ProductosViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [IsRecepcionistaOrIsAdministradorOrIsJefeDeCocina]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsAdministrador]
        return [permission() for permission in permission_classes]