from djongo import models

class Pedido(models.Model):
    
    id = models.AutoField(primary_key=True)
    nombre_cliente=models.CharField(max_length=100)
    lista_productos = models.JSONField()
    estado = models.CharField(max_length=20)