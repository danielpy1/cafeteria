from rest_framework import serializers
from .models import Pedido
from collections import OrderedDict
import json,ast

class PedidoSerializer(serializers.ModelSerializer):   

    class Meta:
        
        model = Pedido
        fields = ['id', 'nombre_cliente', 'lista_productos','estado']