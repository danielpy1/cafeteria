from .models import Pedido
from rest_framework import viewsets
from .serializers import PedidoSerializer
from backend.permissions import IsRecepcionistaOrIsJefeDeCocina,IsRecepcionista
# from productos.models import Producto
# from rest_framework.response import Response
# from rest_framework.decorators import action
# from productos.serializers import ProductoSerializer

class PedidosViewSet(viewsets.ModelViewSet):

    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer

    def get_permissions(self):

        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsRecepcionistaOrIsJefeDeCocina]
        elif self.action == 'destroy' or self.action == 'create':
            permission_classes = [IsRecepcionista]
        return [permission() for permission in permission_classes]


    # def retrieve(self, request, *args, **kwargs):

    #     all_pedidos = Pedido.objects.all()
    #     all_pedidos = Pedido.objects.filter(estado = 'pendiente' or 'listo')
    #     pedidos_pendiente_listo_aux=[]

    #     if(all_pedidos):
    #         for pedido in all_pedidos:
    #             if(pedido.lista_productos):
    #                 lista_producto_detallado=[]
    #                 for id_producto in pedido.lista_productos:
    #                     producto_detallado = Producto.objects.get(pk=id_producto)
    #                     lista_producto_detallado.append(producto_detallado)
    #                 pedido.lista_productos = json.loads(lista_producto_detallado)
    #                 pedidos_pendiente_listo_aux.append(pedido)    

    #     serializer = PedidoSerializer(pedidos_pendiente_listo_aux, many=True)
    #     return Response(serializer.data,status=status.HTTP_200_OK)
    
    # @action(detail=True, methods=['get'])
    # def pedidos(self, request, pk=None):
    #     # logging.basicConfig(filename='example.log',filemode="a", encoding='utf-8', level=logging.DEBUG)
    #     id_pedido = self.kwargs['pk']
    #     if(id_pedido):
    #         try:
    #             pedido = Pedido.objects.get(pk=id_pedido)
    #             lista_productos = []
    #             for jsonFieldListaProducto in pedido.lista_productos:
    #                 for key, value in jsonFieldListaProducto.items():
    #                     print(str(key)+" "+str(value))
    #                     if(key == 'id_producto'):
    #                         lista_productos.append(Producto.objects.get(pk=value))
    #             return Response(ProductoSerializer(lista_productos, many=True).data, status=status.HTTP_200_OK)
    #         except Exception as e:
    #             print(e)
    #             return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
