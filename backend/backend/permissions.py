from rest_framework import permissions
    
class IsRecepcionista(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name == 'recepcionista'
    
class IsJefeDeCocina(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name == 'jefe_de_cocina'
    
class IsAdministrador(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name == 'administrador'
        
class IsRecepcionistaOrIsAdministradorOrIsJefeDeCocina(permissions.BasePermission):
    def has_permission(self, request, view):
        role = request.user.groups.get().name
        return role == 'recepcionista' or role == 'administrador' or role == 'jefe_de_cocina'

class IsRecepcionistaOrIsJefeDeCocina(permissions.BasePermission):
    def has_permission(self, request, view):
        role = request.user.groups.get().name
        return role == 'recepcionista' or role == 'jefe_de_cocina'
    
class IsRecepcionistaOrIsJefeDeCocinaOrIsAdministrador(permissions.BasePermission):
    def has_permission(self, request, view):
        role = request.user.groups.get().name
        return role == 'recepcionista' or role == 'jefe_de_cocina' or role == 'administrador'

