from rest_framework import viewsets
from django.contrib.auth.models import User
from .serializers import UserSerializer
from backend.permissions import IsAdministrador,IsRecepcionistaOrIsJefeDeCocinaOrIsAdministrador
from django.contrib.auth.models import User

class UsersViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdministrador]

    def get_permissions(self):

        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [IsRecepcionistaOrIsJefeDeCocinaOrIsAdministrador]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsAdministrador]

        return [permission() for permission in permission_classes]
    