from rest_framework import serializers
from django.contrib.auth.models import User, Group

class UserSerializer(serializers.ModelSerializer): 

    group_name = serializers.SerializerMethodField()

    class Meta:
        
        model = User
        fields = ('id','username','password', 'group_name')
        extra_kwargs = {'password': {'write_only': True}}

    def get_group_name(self, obj):

        group = Group.objects.filter(user=obj).first()

        return group.name if group else None
    

    def create(self, validated_data, *args, **kwargs):

        user = User.objects.create_user(
            validated_data['username'],
            password = validated_data['password'],
        )
        my_group = Group.objects.get(name=validated_data['group_name'])
        user.groups.add(my_group)

        return user